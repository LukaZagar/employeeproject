<?php

namespace Config;

class Connection
{
    const DB_NAME = 'project';
    const HOSTNAME = 'localhost';
    const USERNAME = 'root';
    const PASSWORD = '';

    private $connection;
    private static $instance;

    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new Connection();
        }
        return self::$instance;
    }

    private function __construct()
    {
        try{
            $this->connection = new \PDO(
                "mysql:host=" .
                self::HOSTNAME .
                ";dbname=" . self::DB_NAME,
                self::USERNAME,
                self::PASSWORD
            );
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE,
                \PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $e){
            die("Connection failed:" . $e->getMessage());
        }

    }

    public function getConnection()
    {
        return $this->connection;
    }

}