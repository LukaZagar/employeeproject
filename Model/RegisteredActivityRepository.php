<?php

namespace Model;

class RegisteredActivityRepository extends EntityRepository
{
    public function deleteRegisteredActivity(RegisteredActivity $registeredActivity)
    {

        $statement = "DELETE FROM registeredactivity WHERE id=" . $registeredActivity->getId();

        $stmt = $this->conn->prepare($statement);
        $stmt->execute();
    }

    public function saveRegisteredActivity(RegisteredActivity $registeredActivity)
    {
        if(!$registeredActivity->getId())
        {
            $statement = "INSERT INTO registeredactivity 
            (activityNumber, name)
            VALUES (:activityNumber, :name)";
        }
        else{
            $statement = "UPDATE registeredactivity SET activityNumber=:activityNumber,
             name=:name WHERE id=" . $registeredActivity->getId();
        }

        $action = $this->conn->prepare($statement);
        $action->bindValue('name', $registeredActivity->getName());
        $action->bindValue('activityNumber', $registeredActivity->getActivityNumber());
        $action->execute();

        return $this->conn->lastInsertId();
    }
}