<?php

namespace Model;

class EmployeeRepository extends  EntityRepository
{
    public function deleteEmployee(Employee $employee)
    {
        $statement = "DELETE FROM employee WHERE id=" . $employee->getId();

        $action = $this->conn->prepare($statement);
        $action->execute();
    }

    public function saveEmployee(Employee $employee)
    {
        if(!$employee->getId())
        {
            $statement = "INSERT INTO employee 
            (firsName, lastName, email, dateOfBirth)
            VALUES (:firstName, :lastNme, :email; :dateOfBirth)";
        }
        else{
            $statement = "UPDATE employee SET firsName=:firstName, lastName=:lastName, 
            email=:email, dateOfBirth=:dateOfBirth WHERE id=" . $employee->getId();
        }

        $action = $this->conn->prepare($statement);

        $action->bindValue('firstName', $employee->getFirstName());
        $action->bindValue('lastName', $employee->getLastName());
        $action->bindValue('email', $employee->getEmail());
        $action->bindValue('dateOfBirth', $employee->getDateOfBirth());

        $action->execute();

        return $this->conn->lastInsertId();
    }
}