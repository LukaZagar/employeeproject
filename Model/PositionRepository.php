<?php

namespace Model;

class PositionRepository extends EntityRepository
{
    public function savePosition(Position $position)
    {
        if(!$position->getId())
        {

            //prvojeri jos  SQL
            $statement = "INSERT INTO position 
            (name)
            VALUES (:name)";
        }
        else{
            $statement = "UPDATE position SET name=:name WHERE id=" . $position->getId();
        }

        $action = $this->conn->prepare($statement);
        $action->bindValue('name', $position->getName());
        $action->execute();

        return $this->conn->lastInsertId();
    }

    public function deletePosition(Position $position)
    {
        $statement = "DELETE FROM user WHERE id=" . $position->getId();

        $stmt = $this->conn->prepare($statement);
        $stmt->execute();
    }
}