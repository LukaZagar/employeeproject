<?php

namespace Model;

class ContractRepository extends EntityRepository
{
    public function saveContract(Contract $contract)
    {
        if(!$contract->getId())
        {

            $statement = "INSERT INTO contract 
            (name, createdAt, path)
            VALUES (:name, :createdAt, :path)";
        }
        else{
            $statement = "UPDATE contract SET name=:name, createdAt=:createdAt, path=:path
            WHERE id=" . $contract->getId();
        }

        $action = $this->conn->prepare($statement);
        $action->bindValue('name', $contract->getName());
        $action->bindValue('createdAt', $contract->getCreatedAt());
        $action->bindValue('path', $contract->getPath());
        $action->execute();

        return $this->conn->lastInsertId();
    }

    public function deleteContract(Contract $contract)
    {
        $statement = "DELETE FROM user WHERE id=" . $contract->getId();

        $stmt = $this->conn->prepare($statement);
        $stmt->execute();
    }
}