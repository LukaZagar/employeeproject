<?php

namespace Model;

class UserRepository extends EntityRepository
{
    public function deleteUser(User $user)
    {
        $statement = "DELETE FROM user WHERE id =" . $user->getId();

        $action = $this->conn->prepare($statement);
        $action->execute();
    }

    public function saveUser(User $user)
    {
        if(!$user->getId())
        {
            $statement = "INSERT INTO user 
          (firstName, lastName, username, password, email)
           VALUES (:firstName, :lastName, :username, :password, :email)";
        }
        else
        {
            $statement = "UPDATE user SET firstName=:firstName, lastName=:lastName,
            username=:username, email=:email, password=:password WHERE id=" . $user->getId();
        }

        $action = $this->conn->prepare($statement);

        $action->bindValue(':firstName', $user->getFirstName());
        $action->bindValue(':lastName', $user->getLastName());
        $action->bindValue(':username', $user->getUsername());
        $action->bindValue(':email', $user->getEmail());
        $action->bindValue(':password', $user->getPassword());
        $action->execute();

        return $this->conn->lastInsertId();
    }
}