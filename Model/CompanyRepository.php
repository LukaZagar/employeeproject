<?php

namespace Model;

class CompanyRepository extends EntityRepository
{
    public function deleteCompany(Company $company)
    {
        $statement = "DELETE FROM company WHERE id=" . $company->getId();

        $action = $this->conn->prepare($statement);
        $action->execute();
    }

    public function saveCompany(Company $company)
    {
        if(!$company->getId())
        {
            $statement = "INSERT INTO company
            (name, registeredAt, taxNumber)
            VALUES (:name, :registeredAt, :taxNumber)";
        }
        else{

            $statement = "UPDATE company SET name=:name, registeredAt=:registeredAt,
            taxNumber=:taxNumber WHERE id=" . $company->getId();
        }

        $action = $this->conn->prepare($statement);

        $action->bindValue('name', $company->getName());
        $action->bindValue('registeredAt', $company->getRegisteredAt());
        $action->bindValue('taxNumber', $company->getTaxNumber());
        $action->execute();

        return $this->conn->lastInsertId();
    }

}