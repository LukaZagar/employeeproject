<?php

namespace Form\EmployeeForm;

use Form\BaseForm;
use Form\Element\Date;
use Form\Element\Email;
use Form\Element\Submit;
use Form\Element\Text;
use Model\Employee;

class UpdateEmployee extends BaseForm
{
    /** @var Employee $employee */
    private $employee;

    public function __construct($employee)
    {
        parent::__construct();
        $this->employee = $employee;
    }

    public function init()
    {
        $firstNameElement = new Text('firstName', 'Vaše ime ovdje');
        $firstNameElement->setPlaceholder("'Unesi ime'");
        $firstNameElement->isRequired(true);
        $firstNameElement->setValue($this->employee->getFirstName());
        $this->addElement($firstNameElement);

        $lastNameElement = new Text('lastName', 'Vaše prezime ovdje');
        $lastNameElement->setPlaceholder("'Unesi prezime'");
        $lastNameElement->isRequired(true);
        $lastNameElement->setValue($this->employee->getLastName());
        $this->addElement($lastNameElement);

        $emailElement = new Email('email', 'Vaš email ovdje');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $emailElement->setValue($this->employee->getEmail());
        $this->addElement($emailElement);

        $dateOfBirthElement = new Date('dateOfBirth', 'Unesi datum rođenja');
        $dateOfBirthElement->setPlaceholder("'Unesi datum rođenja'");
        $dateOfBirthElement->isRequired(true);
        $dateOfBirthElement->setValue($this->employee->getDateOfBirth());
        $this->addElement($dateOfBirthElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}