<?php

namespace Form\EmployeeForm;

use Form\BaseForm;
use Form\Element\Date;
use Form\Element\Email;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterEmployee extends BaseForm
{

    public function init()
    {
        $firstNameElement = new Text('firstName', 'Vaše ime ovdje');
        $firstNameElement->setPlaceholder("'Unesi ime'");
        $firstNameElement->isRequired(true);
        $this->addElement($firstNameElement);

        $lastNameElement = new Text('lastName', 'Vaše prezime ovdje');
        $lastNameElement->setPlaceholder("'Unesi prezime'");
        $lastNameElement->isRequired(true);
        $this->addElement($lastNameElement);

        $emailElement = new Email('email', 'Vaš email ovdje');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $this->addElement($emailElement);

        $dateOfBirthElement = new Date('dateOfBirth', 'Unesi datum rođenja');
        $dateOfBirthElement->setPlaceholder("'Unesi datum rođenja'");
        $dateOfBirthElement->isRequired(true);
        $this->addElement($dateOfBirthElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}