<?php

namespace Form\Element;

abstract class Base
{
    protected $name;
    protected $element;

    private $isRequired;
    private $maxChars;
    private $minChars;

    public function __construct($name, $labelText = null)
    {
        $this->name .= $name;
        $this->element .= "<label>" . $labelText . "</label><br>";
    }

    public function isRequired($required)
    {
        $this->isRequired = $required;
        if($required)
        {
            $this->element .= " required ";
        }

    }

    public function setPlaceholder($placeholder)
    {
        if($placeholder)
        {
            $this->element .= " placeholder=" . $placeholder . " ";
        }

    }

    public function closeElement()
    {
        $this->element .= "/><br> ";
    }

    public function setValue($value)
    {
        $this->element .= " value=" . $value . " ";
    }

    public function __toString()
    {
        return $this->element;
    }

    public function getRequired()
    {
        return $this->isRequired;
    }

    public function getMaxChars()
    {
        return $this->maxChars;
    }

    public function setMaxChars($maxChars)
    {
        $this->maxChars = $maxChars;
    }

    public function getMinChars()
    {
        return $this->minChars;
    }

    public function setMinChars($minChars)
    {
        $this->minChars = $minChars;
    }

    public function getName()
    {
        return $this->name;
    }




}