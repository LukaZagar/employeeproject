<?php

namespace Form\Element;

class Submit extends Base
{

    public function __construct($name = null)
    {
        $this->name = $name;
        $this->element = "<input type='submit'";

        if ($name) {
            $this->element .= " value='" . $name . "' ";
        }

    }


}