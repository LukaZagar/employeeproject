<?php

namespace Form\Element;

class Date extends Base
{
    public function __construct($name = null, $labelText = null)
    {
        parent::__construct($name, $labelText);

        $this->element .= "<input type='date'";

        if($name)
        {
            $this->element .= " name='" . $name . "' ";
        }

    }
}