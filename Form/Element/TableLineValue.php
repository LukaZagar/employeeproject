<?php

namespace Form\Element;

class TableLineValue extends Base
{
    public function __construct($value)
    {
        $this->element = "<td>" . $value . "</td>";
    }

}