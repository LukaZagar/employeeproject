<?php

namespace Form\Element;

class Password extends Base
{

    public function __construct($name = null, $labelText = null)
    {
        parent::__construct($name, $labelText);

        $this->element .= "<input type='password'";

        if ($name) {
            $this->element .= " name='" . $name . "' ";
        }

    }


}