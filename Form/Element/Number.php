<?php

namespace Form\Element;

class Number extends Base
{
    public function __construct($name = null, $labelText = null)
    {
        parent::__construct($name, $labelText);

        $this->element .= "<input type='number'";

        if($name)
        {
            $this->element .= " name='" . $name . "' ";
        }

    }
}