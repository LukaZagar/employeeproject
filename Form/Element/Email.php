<?php

namespace Form\Element;

class Email extends Base
{

    public function __construct($name = null, $labelText = null)
    {

        parent::__construct($name, $labelText);

        $this->element .= "<input type='email'";

        if ($name) {
            $this->element .= " name='" . $name . "' ";
        }

    }


}