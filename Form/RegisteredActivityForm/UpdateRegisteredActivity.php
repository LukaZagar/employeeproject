<?php

namespace Form\RegisteredActivity;

use Form\BaseForm;
use Form\Element\Number;
use Form\Element\Submit;
use Form\Element\Text;
use Model\RegisteredActivity;

class UpdateRegisteredActivity extends BaseForm
{
    /** @var RegisteredActivity as $registeredActivity */
    private $registeredActivity;

    public function __construct($registeredActivity)
    {
        parent::__construct();
        $this->registeredActivity = $registeredActivity;
    }


    public function init()
    {
        $nameElement = new Text('name', 'Ime aktivnosti');
        $nameElement->setPlaceholder("'Unesi ime aktivnosti'");
        $nameElement->isRequired(true);
        $nameElement->setValue($this->registeredActivity->getName());
        $this->addElement($nameElement);

        $activityNumberElement = new Number('activityNumber', 'Broj aktivnosti');
        $activityNumberElement->setPlaceholder('Unesi broj aktivnosti');
        $activityNumberElement->isRequired(true);
        $activityNumberElement->setValue($this->registeredActivity->getActivityNumber());
        $this->addElement($activityNumberElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}