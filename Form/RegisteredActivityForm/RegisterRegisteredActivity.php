<?php

namespace Form\RegisteredActivity;

use Form\BaseForm;
use Form\Element\Number;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterRegisteredActivity extends BaseForm
{
    public function init()
    {
        $nameElement = new Text('name', 'Ime aktivnosti');
        $nameElement->setPlaceholder("'Unesi ime aktivnosti'");
        $nameElement->isRequired(true);
        $this->addElement($nameElement);

        $activityNumberElement = new Number('activityNumber', 'Broj aktivnosti');
        $activityNumberElement->setPlaceholder('Unesi broj aktivnosti');
        $activityNumberElement->isRequired(true);
        $this->addElement($activityNumberElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

         $this->setMethod('POST');
        $this->setAction();
    }
}