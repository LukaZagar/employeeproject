<?php

namespace Form\UserForm;


use Form\Element\TableLineTitle;
use Form\Element\TableLineValue;
use Model\User;

class ReadUser

{
    public function __construct($vars)
    {
        $form = "";

        if(isset($vars['users']))
        {
            $users = $vars['users'];
            $form .= "<table> <thead> <tr>";

            $id = new TableLineTitle('ID');
            $firstName = new TableLineTitle('FirstName');
            $lastName = new TableLineTitle('Lastname');
            $email = new TableLineTitle('Email');
            $username = new TableLineTitle('Username');
            $action = new TableLineTitle('Action');

            $form .= $id . $firstName . $lastName . $email . $username . $action . "</tr></thead><tbody>";

                /** @var User $user */
            foreach ($users as $user)
            {
                $form .= "<tr>";

                $idValue = new TableLineValue($user->getId());
                $firstNameValue = new TableLineValue($user->getFirstName());
                $lastNameValue = new TableLineValue($user->getLastName());
                $emailValue = new TableLineValue($user->getEmail());
                $usernameValue = new TableLineValue($user->getUsername());
                $form .= $idValue . $firstNameValue . $lastNameValue . $emailValue . $usernameValue;
                $form .= " 
                    <td>
                    <a href=\"?userId=<?php echo $user->getId(); ?>\">READ</a>
                    <a href=\"#\">UPDATE</a>
                    <a href=\"#\">DELETE</a>
                </td></tr>";
            }

            $form .= "</tbody></table>";

        }
        else if(isset($vars['user']))
        {
            $user = $vars['user'];
            $form .= "h3>Single user profile</h3>
    <p>
        Id is: <strong><?php echo $user->getId(); ?></strong><br>
        FirstName is: <strong><?php echo $user->getFirstName(); ?></strong><br>
        LastName is: <strong><?php echo $user->getLastName(); ?></strong><br>
        Email is: <strong><?php echo $user->getEmail(); ?></strong><br>
        Username is: <strong><?php echo $user->getUsername(); ?></strong><br>
    </p>";
        }

        echo $form;
    }
}