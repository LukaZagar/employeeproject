<?php

namespace Form\UserForm;

use Form\BaseForm;
use Form\Element\Email;
use Form\Element\Password;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterUser extends BaseForm
{

    public function init()
    {
        $firstNameElement = new Text('firstName', 'Vaše ime ovdje');
        $firstNameElement->setPlaceholder("'Unesi ime'");
        $firstNameElement->isRequired(true);
        $this->addElement($firstNameElement);

        $lastNameElement = new Text('lastName', 'Vaše prezime ovdje');
        $lastNameElement->setPlaceholder("'Unesi prezime'");
        $lastNameElement->isRequired(true);
        $this->addElement($lastNameElement);

        $emailElement = new Email('email', 'Vaš email ovdje');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $this->addElement($emailElement);

        $usernameElement = new Text('username', 'Vaše korisničko ime');
        $usernameElement->setPlaceholder("'Unesi korisničko ime'");
        $usernameElement->isRequired(true);
        $this->addElement($usernameElement);

        $passwordElement = new Password('password', 'Unesite lozinku');
        $passwordElement->setPlaceholder("'Unesi lozinku'");
        $passwordElement->isRequired(true);
        $this->addElement($passwordElement);

        $repeatPasswordElement = new Password('repeatPassword', 'Unesite ponovno lozinku');
        $repeatPasswordElement->setPlaceholder("'Unesi ponovno lozinku'");
        $repeatPasswordElement->isRequired(true);
        $this->addElement($repeatPasswordElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }


}