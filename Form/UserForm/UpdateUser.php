<?php

namespace Form\UserForm;

use Form\BaseForm;
use Form\Element\Email;
use Form\Element\Password;
use Form\Element\Submit;
use Form\Element\Text;
use Model\User;

class UpdateUser extends BaseForm
{
     /** @var User $user*/
    private $user;

    public function __construct($user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function init()
    {
        $firstNameElement = new Text('firstName', 'Vaše ime ovdje');
        $firstNameElement->setPlaceholder("'Unesi ime'");
        $firstNameElement->isRequired(true);
        $firstNameElement->setValue($this->user->getFirstName());
        $this->addElement($firstNameElement);

        $lastNameElement = new Text('lastName', 'Vaše prezime ovdje');
        $lastNameElement->setPlaceholder("'Unesi prezime'");
        $lastNameElement->isRequired(true);
        $lastNameElement->setValue($this->user->getLastName());
        $this->addElement($lastNameElement);

        $emailElement = new Email('email', 'Vaš email ovdje');
        $emailElement->setPlaceholder("'Unesi email'");
        $emailElement->isRequired(true);
        $emailElement->setValue($this->user->getEmail());
        $this->addElement($emailElement);

        $usernameElement = new Text('username', 'Vaše korisničko ime');
        $usernameElement->setPlaceholder("'Unesi korisničko ime'");
        $usernameElement->isRequired(true);
        $usernameElement->setValue($this->user->getUsername());
        $this->addElement($usernameElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}