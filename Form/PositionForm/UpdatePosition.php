<?php

namespace Form\PositionForm;

use Form\BaseForm;
use Form\Element\Submit;
use Form\Element\Text;
use Model\Position;

class UpdatePosition extends BaseForm
{
    /** @var Position $position */
    private $position;

    public function __construct($position)
    {
        parent::__construct();
        $this->position = $position;
    }


    public function init()
    {
        $nameElement = new Text('name', 'Ime pozicije');
        $nameElement->setPlaceholder("'Unesi ime pozicije'");
        $nameElement->isRequired(true);
        $nameElement->setValue($this->position->getName());
        $this->addElement($nameElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}