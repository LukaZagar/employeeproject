<?php

namespace Form\PositionForm;

use Form\BaseForm;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterPosition extends  BaseForm
{
    public function init()
    {
        $nameElement = new Text('name', 'Ime pozicije');
        $nameElement->setPlaceholder("'Unesi ime pozicije'");
        $nameElement->isRequired(true);
        $this->addElement($nameElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }


}