<?php

namespace Form\ContractForm;

use Form\BaseForm;
use Form\Element\Date;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterContract extends BaseForm
{
    public function init()
    {
        $nameElement = new Text('name', 'Ime ugovora');
        $nameElement->setPlaceholder("'Unesi ime ugovora'");
        $nameElement->isRequired(true);
        $this->addElement($nameElement);

        $registeredAtElement = new Date('createdAt', 'Datum ugovora');
        $registeredAtElement->setPlaceholder("'Unesi datum'");
        $registeredAtElement->isRequired(true);
        $this->addElement($registeredAtElement);

        $pathElement = new Text('path', 'Putanja');
        $pathElement->setPlaceholder("'Unesi putanju'");
        $pathElement->isRequired(true);
        $this->addElement($pathElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }

}