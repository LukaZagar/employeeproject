<?php

namespace Form\ContractForm;

use Form\BaseForm;
use Model\Contract;
use Form\Element\Date;
use Form\Element\Submit;
use Form\Element\Text;

class UpdateContract extends BaseForm
{
    /** @var Contract $contract */
    private $contract;

    public function __construct($contract)
    {
        parent::__construct();
        $this->contract = $contract;
    }

    public function init()
    {
        $nameElement = new Text('name', 'Ime ugovora');
        $nameElement->setPlaceholder("'Unesi ime ugovora'");
        $nameElement->isRequired(true);
        $nameElement->setValue($this->contract->getName());
        $this->addElement($nameElement);

        $createdAtElement = new Date('createdAt', 'Datum ugovora');
        $createdAtElement->setPlaceholder("'Unesi datum'");
        $createdAtElement->isRequired(true);
        $createdAtElement->setValue($this->contract->getCreatedAt());
        $this->addElement($createdAtElement);

        $pathElement = new Text('path', 'Putanja');
        $pathElement->setPlaceholder("'Unesi putanju'");
        $pathElement->isRequired(true);
        $pathElement->setValue($this->contract->getPath());
        $this->addElement($pathElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}