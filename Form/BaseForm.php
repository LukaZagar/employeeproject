<?php

namespace Form;

use Form\Element\Base;

abstract class BaseForm
{
    private $method;
    private $action;

    private $errorMessages = array();

    private $elements = array();
    private $plainElements = array();

    abstract function init();

    public function __construct()
    {
        $this->init();
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setMethod($method)
    {
        $this->method = $method;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action = null)
    {
        $this->action = $action;
    }

    public function addElement(Base $element)
    {
        $element->closeElement();

        $this->plainElements[$element->getName()] = $element;
        $this->elements[$element->getName()] = $element->__toString();
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function getElementObjects()
    {
        return $this->plainElements;
    }

    public function getElement($name)
    {
        if(isset($this->elements[$name]))
        {
            return $this->elements[$name];
        }
        return null;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setErrorMessages($errorMessage)
    {
        $this->errorMessages[] = $errorMessage;
    }



}