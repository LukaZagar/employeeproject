<?php

namespace Form\CompanyForm;

use Form\BaseForm;
use Form\Element\Date;
use Form\Element\Number;
use Form\Element\Submit;
use Form\Element\Text;
use Model\Company;

class UpdateCompany extends BaseForm
{
    /** @var Company $company */
    private $company;

    public function __construct($company)
    {
        parent::__construct();
        $this->company = $company;
    }

    public function init()
    {
        $nameElement = new Text('name', 'Ime kompanije');
        $nameElement->setPlaceholder("'Unesi ime kompanije'");
        $nameElement->isRequired(true);
        $nameElement->setValue($this->company->getName());
        $this->addElement($nameElement);

        $registeredAtElement = new Date('registeredAt', 'Datum registriranja firme');
        $registeredAtElement->setPlaceholder("'Unesi datum'");
        $registeredAtElement->isRequired(true);
        $registeredAtElement->setValue($this->company->getRegisteredAt());
        $this->addElement($registeredAtElement);

        $taxNumberElement = new Number('taxNumber', 'Porezni broj');
        $taxNumberElement->setPlaceholder("'Unesi porezni broj'");
        $taxNumberElement->isRequired(true);
        $taxNumberElement->setValue($this->company->getTaxNumber());
        $this->addElement($taxNumberElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }
}