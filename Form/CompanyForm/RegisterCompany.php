<?php

namespace Form\CompanyForm;

use Form\BaseForm;
use Form\Element\Date;
use Form\Element\Number;
use Form\Element\Submit;
use Form\Element\Text;

class RegisterCompany extends BaseForm
{
    public function init()
    {
        $nameElement = new Text('name', 'Ime kompanije');
        $nameElement->setPlaceholder("'Unesi ime kompanije'");
        $nameElement->isRequired(true);
        $this->addElement($nameElement);

        $registeredAtElement = new Date('registeredAt', 'Datum registriranja firme');
        $registeredAtElement->setPlaceholder("'Unesi datum'");
        $registeredAtElement->isRequired(true);
        $this->addElement($registeredAtElement);

        $taxNumberElement = new Number('taxNumber', 'Porezni broj');
        $taxNumberElement->setPlaceholder("'Unesi porezni broj'");
        $taxNumberElement->isRequired(true);
        $this->addElement($taxNumberElement);

        $submitElement = new Submit('Submit');
        $this->addElement($submitElement);

        $this->setMethod('POST');
        $this->setAction();
    }

}