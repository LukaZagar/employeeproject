<?php

namespace Controller;

use Form\UserForm\RegisterUser;
use Form\UserForm\UpdateUser;
use Model\User;
use Model\UserRepository;

class UserController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $user = new User();
            $user->setFirstName($_POST['firstName']);
            $user->setLastName($_POST['lastName']);
            $user->setUsername($_POST['username']);
            $user->setEmail($_POST['email']);
            $user->hashAndSetPassword($_POST['password']);

            $userRepository = new UserRepository();
            $userRepository->saveUser($user);
        }
        $form = new RegisterUser();
        $this->render('user:create', $form);

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $userId = $_GET['id'];
        $userRepository = new UserRepository();
        $user = $userRepository->getById($userId);

        if (!$user) {
            die('nema usera');
        }

        if ($_POST) {

            $user->setFirstName($_POST['firstName']);
            $user->setLastName($_POST['lastName']);
            $user->setUsername($_POST['username']);
            $user->setEmail($_POST['email']);

            $userRepository->saveUser($user);
        }
        $form = new UpdateUser($user);
        $this->render('user:update', $form);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $userId = $_GET['id'];
        $userRepository = new UserRepository();
        $user = $userRepository->getById($userId);

        if(!$user)
        {
            die("nema usera");
        }

        $userRepository->deleteUser($user);

        $this->render('user:delete', $user);
    }

    public function readAction()
    {
        $userId = isset($_GET['userId']) ? $_GET['userId'] : null;
        $userRepository = new UserRepository();
        if($userId)
        {
            $vars['user'] = $userRepository->getById($userId);
        }
        else
        {
            $vars['users'] = $userRepository->getAll();
        }

        $this->render('user:read', $vars);
    }

}