<?php

namespace Controller;

use Model\Position;
use Model\PositionRepository;

class PositionController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $position = new Position();

            $positionRepository = new PositionRepository();
            $positionRepository->savePosition($position);
        }

        $this->render('position:create');

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $positionId = $_GET['id'];
        $positionRepository = new PositionRepository();
        $position = $positionRepository->getById($positionId);

        if (!$position) {
            die('nema trazene pozicije');
        }

        if ($_POST) {

            $positionRepository->savePosition($position);
        }

        $this->render('position:update', $position);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $positionId = $_GET['id'];
        $positionRepository = new PositionRepository();
        $position = $positionRepository->getById($positionId);

        if(!$position)
        {
            die("nema zaposlenika");
        }

        $positionRepository->deletePosition($position);

        $this->render('position:delete', $position);
    }

    public function readAction()
    {
        $positionId = isset($_GET['positionId']) ? $_GET['positionId'] : null;
        $positionRepository = new PositionRepository();
        if($positionId)
        {
            $vars['position'] = $positionRepository->getById($positionId);
        }
        else
        {
            $vars['positions'] = $positionRepository->getAll();
        }

        $this->render('position:read', $vars);
    }
}