<?php

namespace Controller;

use Model\Contract;
use Model\ContractRepository;

class ContractController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $contract = new Contract();
            $contract->setName($_POST['name']);
            $contract->setCreatedAt($_POST['createdAt']);
            $contract->setPath($_POST['path']);

            $contractRepository = new ContractRepository();
            $contractRepository->saveContract($contract);
        }

        $this->render('contract:create');

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $contractId = $_GET['id'];
        $contractRepository = new ContractRepository();
        $contract = $contractRepository->getById($contractId);

        if (!$contract) {
            die('nema ugovora');
        }

        if ($_POST) {

            $contract->setName($_POST['name']);
            $contract->setCreatedAt($_POST['createdAt']);
            $contract->setPath($_POST['path']);

            $contractRepository->saveContract($contract);
        }

        $this->render('contract:update', $contract);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $contractId = $_GET['id'];
        $contractRepository = new ContractRepository();
        $contract = $contractRepository->getById($contractId);

        if(!$contract)
        {
            die("nema ugovora");
        }

        $contractRepository->deleteContract($contract);

        $this->render('contract:delete', $contract);
    }

    public function readAction()
    {
        $contractId = isset($_GET['contractId']) ? $_GET['contractId'] : null;
        $contractRepository = new ContractRepository();
        if($contractId)
        {
            $vars['contract'] = $contractRepository->getById($contractId);
        }
        else
        {
            $vars['contracts'] = $contractRepository->getAll();
        }

        $this->render('contract:read', $vars);
    }
}