<?php

namespace Controller;

use Model\RegisteredActivity;
use Model\RegisteredActivityRepository;

class RegisteredActivityController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $registeredActivity = new RegisteredActivity();
            $registeredActivity->setName($_POST['name']);
            $registeredActivity->setActivityNumber($_POST['activityNumber']);

            $registeredActivityRepository = new RegisteredActivityRepository();
            $registeredActivityRepository->saveRegisteredActivity($registeredActivity);
        }

        $this->render('registeredActivity:create');

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $registeredActivityId = $_GET['id'];
        $registeredActivityRepository = new RegisteredActivityRepository();
        $registeredActivity = $registeredActivityRepository->getById($registeredActivityId);

        if (!$registeredActivity) {
            die('nema aktivnosti');
        }

        if ($_POST) {

            $registeredActivity->setName($_POST['name']);
            $registeredActivity->setActivityNumber($_POST['activityNumber']);

            $registeredActivityRepository->saveRegisteredActivity($registeredActivity);
        }

        $this->render('registeredActivity:update', $registeredActivity);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $registeredActivityId = $_GET['id'];
        $registeredActivityRepository = new RegisteredActivityRepository();
        $registeredActivity = $registeredActivityRepository->getById($registeredActivityId);

        if(!$registeredActivity)
        {
            die("nema aktivnosti");
        }

        $registeredActivityRepository->deleteRegisteredActivity($registeredActivity);

        $this->render('registeredActivity:delete', $registeredActivity);
    }

    public function readAction()
    {
        $registeredActivityId = isset($_GET['registeredActivityId']) ? $_GET['registeredActivityId'] : null;
        $registeredActivityRepository = new RegisteredActivityRepository();
        if($registeredActivityId)
        {
            $vars['registeredActivity'] = $registeredActivityRepository->getById($registeredActivityId);
        }
        else
        {
            $vars['registeredActivities'] = $registeredActivityRepository->getAll();
        }

        $this->render('registeredActivity:read', $vars);
    }
}