<?php

namespace Controller;

use Model\Company;
use Model\CompanyRepository;

class CompanyController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $company = new Company();
            $company->setName($_POST['name']);
            $company->setRegisteredAt($_POST['registeredAt']);
            $company->setTaxNumber($_POST['taxNumber']);

            $companyRepository = new CompanyRepository();
            $companyRepository->saveCompany($company);
        }

        $this->render('company:create');

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $companyId = $_GET['id'];
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->getById($companyId);

        if (!$company) {
            die('nema kompanije');
        }

        if ($_POST) {

            $company->setName($_POST['name']);
            $company->setRegisteredAt($_POST['registeredAt']);
            $company->setTaxNumber($_POST['taxNumber']);

            $companyRepository->saveCompany($company);
        }

        $this->render('company:update', $company);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $companyId = $_GET['id'];
        $companyRepository = new CompanyRepository();
        $company = $companyRepository->getById($companyId);

        if(!$company)
        {
            die("nema kompanije");
        }

        $companyRepository->deleteCompany($company);

        $this->render('company:delete', $company);
    }

    public function readAction()
    {
        $companyId = isset($_GET['companyId']) ? $_GET['companyId'] : null;
        $companyRepository = new CompanyRepository();
        if($companyId)
        {
            $vars['company'] = $companyRepository->getById($companyId);
        }
        else
        {
            $vars['companies'] = $companyRepository->getAll();
        }

        $this->render('company:read', $vars);
    }
}