<?php

namespace  Controller;

use Model\Employee;
use Model\EmployeeRepository;

class EmployeeController extends BaseController
{
    public function createAction()
    {
        if($_POST)
        {
            $employee = new Employee();
            $employee->setFirstName($_POST['firstName']);
            $employee->setLastName($_POST['lastName']);
            $employee->setEmail($_POST['email']);
            $employee->setDateOfBirth($_POST['dateOfBirth']);

            $employeeRepository = new EmployeeRepository();
            $employeeRepository->saveEmployee($employee);
        }

        $this->render('employee:create');

    }

    public function updateAction()
    {
        if (!isset($_GET['id']))
        {
            die("Nema ID-a");
        }

        $employeeId = $_GET['id'];
        $employeeRepository = new EmployeeRepository();
        $employee = $employeeRepository->getById($employeeId);

        if (!$employee) {
            die('nema zaposlenika');
        }

        if ($_POST) {

            $employee->setFirstName($_POST['firstName']);
            $employee->setLastName($_POST['lastName']);
            $employee->setEmail($_POST['email']);
            $employee->setDateOfBirth($_POST['dateOfBirth']);

            $employeeRepository->saveEmployee($employee);
        }

        $this->render('employee:update', $employee);

    }

    public function deleteAction()
    {
        if(!isset($_GET['id']))
        {
            die("nema id-a");
        }

        $employeeId = $_GET['id'];
        $employeeRepository = new EmployeeRepository();
        $employee = $employeeRepository->getById($employeeId);

        if(!$employee)
        {
            die("nema zaposlenika");
        }

        $employeeRepository->deleteEmployee($employee);

        $this->render('employee:delete', $employee);
    }

    public function readAction()
    {
        $employeeId = isset($_GET['employeeId']) ? $_GET['employeeId'] : null;
        $employeeRepository = new EmployeeRepository();
        if($employeeId)
        {
            $vars['employee'] = $employeeRepository->getById($employeeId);
        }
        else
        {
            $vars['employees'] = $employeeRepository->getAll();
        }

        $this->render('employee:read', $vars);
    }
}